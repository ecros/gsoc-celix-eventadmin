/**
 *Licensed to the Apache Software Foundation (ASF) under one
 *or more contributor license agreements.  See the NOTICE file
 *distributed with this work for additional information
 *regarding copyright ownership.  The ASF licenses this file
 *to you under the Apache License, Version 2.0 (the
 *"License"); you may not use this file except in compliance
 *with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing,
 *software distributed under the License is distributed on an
 *"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 *specific language governing permissions and limitations
 *under the License.
 */
/*
 * event_admin_impl.c
 *
 *  Created on: Jul 24, 2013
 *      Author: erik
 *  	copyright	Apache License, Version 2.0
 *		note: Create for Google Summer of Code 2013
 */

#include "event_admin.h"
#include "event_admin_impl.h"
#include "event_handler.h"
#include "hash_map.h"
#include "utils.h"


celix_status_t eventAdminCreate(apr_pool_t *pool, bundle_context_pt context, event_admin_pt *event_admin){
	celix_status_t status = CELIX_SUCCESS;
	*event_admin = apr_palloc(pool, sizeof(**event_admin));
	if(!*event_admin){
				status = CELIX_ENOMEM;
		}else {
			(*event_admin)->pool = pool;
			(*event_admin)->channels = hashMap_create(utils_stringHash, utils_stringHash, utils_stringEquals, utils_stringEquals);
			(*event_admin)->context =context;

			status = arrayList_create(&(*event_admin)->event_handlers);
		}
	return status;
}

celix_status_t getEventHandlersByChannel(bundle_context_pt context, const char * serviceName, array_list_pt *eventHandlers) {
	celix_status_t status = CELIX_SUCCESS;
	status = bundleContext_getServiceReferences(context, serviceName,NULL, eventHandlers);
	return status;
}

celix_status_t eventAdminPostEvent(event_admin_pt event_admin, event_pt event){
	celix_status_t status = CELIX_SUCCESS;
	array_list_pt event_handlers = NULL;
	char *topic;
		getTopic(&event,&topic);
	eventAdminLockHandlersList(event_admin,topic);
	printf("post called\n");
	eventAdminReleaseHandersList(event_admin,topic);
	return status;
}

celix_status_t eventAdminSendEvent(event_admin_pt event_admin, event_pt event){
	celix_status_t status = CELIX_SUCCESS;
	printf("send called\n");
	char *topic;
	getTopic(&event,&topic);

	array_list_pt event_handlers;
	arrayList_create(&event_handlers);
	eventAdminLockHandlersList(event_admin,topic);
	eventAdminFindHandlersByTopic(event_admin->channels,topic,event_handlers);
	array_list_iterator_pt handlers_itterator = arrayListIterator_create(event_handlers);
	while(arrayListIterator_hasNext(handlers_itterator)){
		void *handler = arrayListIterator_next(handlers_itterator);
		event_handler_service_pt event_handler_service = (event_handler_service_pt) handler;
		event_handler_service->handle_event(&event_handler_service->event_handler,event);
		printf("handler found \n");
	}
	eventAdminReleaseHandersList(event_admin,topic);
	return status;
}



celix_status_t eventAdminFindHandlersByTopic(hash_map_pt channels, char *topic , array_list_pt event_handlers){
	celix_status_t status = CELIX_SUCCESS;
	char *channel_name;
	if ((channel_name = strsep(&topic, "/")) != NULL){
		if(hashMap_containsKey(channels,channel_name)==1) {
			channel_t channel = hashMap_get(channels,channel_name);
			if(channel->eventHandlers != NULL && hashMap_size(channel->eventHandlers)> 0 ){
				hash_map_iterator_pt hashmap_itterator =  hashMapIterator_create(channel->eventHandlers);
				while(hashMapIterator_hasNext(hashmap_itterator)){
					event_handler_service_pt event_handler_service = NULL;
					event_handler_service = (event_handler_service_pt) hashMapIterator_nextValue(hashmap_itterator);
					arrayList_add(event_handlers,event_handler_service);
				}
			}
			if(topic != NULL){
				status =eventAdminFindHandlersByTopic(channel->channels,topic,event_handlers);
			}
		}
	}
	return status;
}



celix_status_t eventAdminCreateEventChannelsByEventHandler(apr_pool_t *pool,event_handler_service_pt event_handler_service, char *topic, channel_t *channel){
	celix_status_t status = CELIX_SUCCESS;
	char *channel_name;
	if ((channel_name = strsep(&topic, "/")) != NULL){
		*channel = apr_palloc(pool, sizeof(**channel));
		if(!*channel){
			status = CELIX_ENOMEM;
		}else {
			(*channel)->topic = channel_name;
			(*channel)->eventHandlers = hashMap_create(NULL,NULL,NULL,NULL);
			(*channel)->channels = hashMap_create(utils_stringHash, utils_stringHash, utils_stringEquals, utils_stringEquals);
			(*channel)->channelLock = NULL;
			apr_thread_mutex_create(&(*channel)->channelLock,APR_THREAD_MUTEX_NESTED, pool);
		}
		if(topic == NULL && status == CELIX_SUCCESS){
			hashMap_put((*channel)->eventHandlers,&event_handler_service,event_handler_service);
		} else if(status == CELIX_SUCCESS){
			channel_t subChannel=NULL;
			apr_pool_t *subPool= NULL;
			apr_pool_create(&subPool,pool);
			if(subPool == NULL){
				status = CELIX_ENOMEM;
			}else {
				status = eventAdminCreateEventChannelsByEventHandler(subPool,event_handler_service,topic,&subChannel);
				if(status == CELIX_SUCCESS){
					hashMap_put((*channel)->channels,subChannel->topic,subChannel);
				}else {
					status = CELIX_BUNDLE_EXCEPTION;
				}
			}
		}
	}
	return status;
	}

celix_status_t eventAdminLockHandlersList(event_admin_pt event_admin, char *topic ){
	celix_status_t status = CELIX_SUCCESS;
	char *channel_name;
	printf("lock: %s\n",topic);
	/*if ((channel_name = strsep(&topic, "/")) != NULL){
		printf("lock: %s\n",channel_name);
		if(hashMap_containsKey(event_admin->channels,channel_name)==1) {
			channel_t channel = hashMap_get(event_admin->channels,channel_name);
			apr_thread_mutex_trylock(channel->channelLock);
		}
	}
*/
	printf("LOCK!\n");
	return status;
}
celix_status_t eventAdminReleaseHandersList(event_admin_pt event_admin, char *topic ){
	celix_status_t status = CELIX_SUCCESS;
	char *channel_name;
	printf("release: %s\n",topic);
	/*	if ((channel_name = strsep(&topic, "/")) != NULL){
			printf("release: %s\n",channel_name);
			if(hashMap_containsKey(event_admin->channels,channel_name)==1) {
				channel_t channel = hashMap_get(event_admin->channels,channel_name);
				apr_thread_mutex_unlock(channel->channelLock);
			}
		}*/
	printf("UNLOCK\n");
	return status;
}

celix_status_t eventAdminAddingService(void * handle, service_reference_pt ref, void **service) {

	celix_status_t status = CELIX_SUCCESS;
	event_admin_pt  event_admin = handle;
	status = bundleContext_getService(event_admin->context, ref, service);
  	return CELIX_SUCCESS;
}

celix_status_t eventAdminAddedService(void * handle, service_reference_pt ref, void * service) {
	celix_status_t status = CELIX_SUCCESS;
	event_admin_pt  event_admin = handle;
	event_handler_service_pt event_handler_service = NULL;
	event_handler_service = (event_handler_service_pt) service;
	service_registration_pt registration = NULL;
	serviceReference_getServiceRegistration(ref,&registration);
	properties_pt props = NULL;
	serviceRegistration_getProperties(registration, &props);
	char *topic = properties_get(props, (char*)EVENT_TOPIC);
	channel_t channel=NULL;
	apr_pool_t *newPool= NULL;
	apr_pool_create(&newPool,event_admin->pool);
	eventAdminCreateEventChannelsByEventHandler(newPool,event_handler_service,topic,&channel);
	hashMap_put(event_admin->channels,topic,channel);
	return CELIX_SUCCESS;
}

celix_status_t eventAdminModifiedService(void * handle, service_reference_pt ref, void * service) {
	struct data * data = (struct data *) handle;
	printf("Event admin Modified\n");
	return CELIX_SUCCESS;
}

celix_status_t eventAdminRemovedService(void * handle, service_reference_pt ref, void * service) {
	struct data * data = (struct data *) handle;
	printf("Event admin Removed %p\n", service);
	return CELIX_SUCCESS;
}

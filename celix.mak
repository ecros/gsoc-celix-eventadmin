PACKAGE=celix
VERSION=0.0
REVISION=1556864
MAKEFILE=celix.mak

DEPS_HOME=../../../export/

all: buildall

buildall:  build_linux build_linux64

build_linux:
	export PLATFORM=linux; \
	export CC=gcc CFLAGS=-m32 CXXFLAGS=-m32 LDFLAGS=-m32; \
	$(MAKE) -f $(MAKEFILE) build

build_linux64:
	export PLATFORM=linux64; \
	export CC=gcc CFLAGS=-m64 CXXFLAGS=-m64 LDFLAGS=-m64; \
	$(MAKE) -f $(MAKEFILE) build

build:
	rm -rf gen/$(PACKAGE)/$(PLATFORM);\
	mkdir -p gen/$(PACKAGE)/$(PLATFORM);\
	mkdir -p export/$(PACKAGE)/$(PLATFORM);\
	cd gen/$(PACKAGE)/$(PLATFORM);\
	cmake ../../../celix \
		-DAPR_INCLUDE_DIR=$(DEPS_HOME)apr/$(PLATFORM)/include/apr-1 \
		-DAPR_LIBRARY=$(DEPS_HOME)apr/$(PLATFORM)/lib/libapr-1.so \
		-DAPRUTIL_INCLUDE_DIR=$(DEPS_HOME)apr-util/$(PLATFORM)/include/apr-1 \
		-DAPRUTIL_LIBRARY=$(DEPS_HOME)apr-util/$(PLATFORM)/lib/libaprutil-1.so \
		-DCURL_LIBRARY=$(DEPS_HOME)curl/$(PLATFORM)/lib/libcurl.so \
        -DCURL_INCLUDE_DIR=$(DEPS_HOME)curl/$(PLATFORM)/include \
        -DZLIB_LIBRARY=$(DEPS_HOME)zlib/$(PLATFORM)/lib/libz.so \
		-DBUILD_DEVICE_ACCESS=ON \
		-DBUILD_EVENT_ADMIN=ON \
        -DBUILD_RSA_BUNDLES=ON \
        -DBUILD_RSA_BUNDLES_DISCOVERY_BONJOUR=OFF \
        -DBUILD_RSA_BUNDLES_DISCOVERY_SLP=OFF \
        -DBUILD_RSA_BUNDLES_REMOTE_SERVICE_ADMIN_HTTP=OFF \
		-DCMAKE_C_COMPILER=$(CC) -DCMAKE_INSTALL_PREFIX=`pwd`/$(DEPS_HOME)$(PACKAGE)/$(PLATFORM);\
	$(MAKE) install;\

cleanall:
	rm -rf gen